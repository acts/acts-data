brief info for FCChhBField_rz.txt
file format           : txt
coordinate system     : rz
Dimensions            : r[0.,20.],z[0.,30.]
Length units          : m
bField unit           : T
nBins                 : [201,301]
first quadrant/octant : true

brief info for FCChhBField_rz.root
file format           : root
coordinate system     : rz
Dimensions            : r[0.,20000.],z[0.,30000.]
Length units          : mm
bField unit           : T
nBins                 : [201,301]
first quadrant/octant : true

The file ‘FCChhBField_rz.txt’ was created on 15.08.2017 from the model ‘4T10m solenoid - forward solenoid, 20m + 2x4m, 10-17-16_ERWIN.mph’ (Feb 23 2017). It contains the the first quadrant (can be symmetrically extended for the other quadrants) of the magnetic flux density in cylinder coordinates for different positions. The first row gives the r position, the second the z position, the third the magnetic flux density in r (Br), the last row is the magnetic flux density in z (Bz). The maximum in r position is 20m and in z position is 30m. The positions are given in 0.1m steps and  have 201 bins in r and 301 bins in z (for one quadrant). The length unit is meter (m) and the unit of the magnetic flux density is Tesla(T).

The file ‘FCChhBField_rz.root’ was created on 30.01.2018 from the the file ‘FCChhBField_rz.txt’ using the script 'acts-data/MagneticField/translateBFieldMap.cpp' with the following command:

translateBFieldMap("FCChhBField.root","FCChhBField_rz.root","bField",true,1.,1000.)

It contains the first quadrant (can be symmetrically extended for the other quadrants) of the magnetic flux density in cylinder coordinates for different positions, spanning from 0m to 20m in r and from -30m to 30m in z. The positions are given in 0.1m steps and  have 201 bins in r and 301 bins in z (for the first quadrant). The length unit is millimeter (mm) and the unit of the magnetic flux density is Tesla(T). The file contains a tree ‘bField’ which has the following branches describing the coordinates of the positions and the different components of the magnetic flux density: ’r’,’z’,,’Br’,’Bz’.

