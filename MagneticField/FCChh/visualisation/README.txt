Thiss repository contains plots and pictures of the FCChh magnetic field.

The file 'FCChhBField_xyz.root' was created on 30.01.2018 from ‘FCChhBField_rz.root' using the 'InterpolatedBFieldWriter' of 'acts-framework'. It contains a tree of the FCChh BField in xyz-coordinates.

./build/bin/ACTFWBFieldExample --bf-map="../acts-data/MagneticField/FCChh/FCChhBField_rz.root" --bf-rz=1 --bf-file-out="FCChhBField_xyz.root" --bf-foctant=1 --bf-out-rz=0


The file 'FCChhField_xyz_Map.root' was created from 'FCChhField.root' using the root script 'printBField' of 'acts-framework':

printBField("FCChhBField_xyz.root","bField","FCChhField_xyz_Map.root",-20.,20.,-30.,30.,200.)

It contains a histogram of the FCChh magnetic field map in xy, zy and zx.


The file 'FCChhBField_Map.root' was created from ‘FCChhField_rz.root' using the root script 'printBField' of 'acts-framework' :

printBField("./acts-data/MagneticField/FCChh/FCChhField_rz.root","bField","FCChhBField_Map.root",-20.,20.,-30.,30.,300.)

It contains histograms of the FCChh magnetic field map in rz.

The files ‘FCChhBField_rz.png', ‘FCChhBField_xy.png', ‘FCChhBField_zr.png', ‘FCChhBField_zx.png' and ‘FCChhBField_zy.png' are screen shots of the above histograms.
