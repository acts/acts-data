brief info for ATLASBField_xyz.txt
file format           : txt
coordinate system     : xyz
Dimensions            : x[-10000.,10000.],y[-10000.,10000.],z[-15000.,15000.]
Length units          : mm
bField unit           : T
nBins                 : [201,201,301]
first quadrant/octant : false

brief info for FCChhBField_rz.root
file format           : root
coordinate system     : xyz
Dimensions            : x[-10000.,10000.],y[-10000.,10000.],z[-15000.,15000.]
Length units          : mm
bField unit           : T
nBins                 : r[201,201,301]
first quadrant/octant : false


The file ‘ATLASBField_xyz.txt’ was created on 21.10.2016. It contains full magnetic field map (all octants) of the magnetic flux density in cartesian coordinates for different positions, spanning from -10m to 10m in x/y and from -15m to 15m in z. . The first row gives the x position, the second the y position, the third the z position, the fourth the magnetic flux density in x (Bx), the fith the magnetic flux density in y (By), the last row is the magnetic flux density in z (Bz). The positions are given in 0.1m steps and  have 201 bins in x/y and 301 bins in z. The length unit is millimeter (mm) and the unit of the magnetic flux density is Tesla(T).

The file ‘ATLASBField_xyz.root’ was created on 29.01.2018y from the the file ‘ATLASBField_xyz.txt’ using the script 'acts-data/MagneticField/translateBFieldMap.cpp' with the following command:

translateBFieldMap("ATLASBField_xyz.txt","ATLASBField_xyz.root","bField",false,1.,1.)

It contains the full magnetic field map (all octants) of the magnetic flux density in cartesian coordinates for different positions, spanning from -10m to 10m in x/y and from -15m to 15m in z. The positions are given in 0.1m steps and  have 201 bins in r/x/yand 301 bins in z. The length unit is millimeter (mm) and the unit of the magnetic flux density is Tesla(T). The file contains a tree ‘bField’ which has the following branches describing the coordinates of the positions and the different components of the magnetic flux density: ‘x’,’y’,’z’,’Bx’,’By’,’Bz’.

