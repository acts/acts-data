27/07/17 (Geant4) & 26/09/17 (ACTS repeated after rebasing)

Validation of energy loss of Fatras simulation compared to Geant4 simulation.
The Fatras plots (excells_charged_*.root) are the output of the FatrasGunDD4hep.cpp using the following command:
./build/bin/ACTFWFatrasGunDD4hep --dd4hep-input=file:Detectors/DD4hepDetector/compact/DummyDetector.xml --pg-phirange={0.,0.} --pg-etarange={0.,0.} --events=100 --pg-nparticles=100 --loglevel=2 --pg-ptrange={500.,500.}
and the Geant4 files (G4ParticleProperties*.root) are the output of the FatrasValidation.cpp example.

In order to only test energy loss  only the EnergyLossSampler (using function MaterialInteraction::ionizationEnergyLoss
which uses PDG formula 32.11 for MOP value from http://http://pdg.lbl.gov/2014/reviews/rpp2014-rev-passage-particles-matter.pdf)
from the acts-fatras library has beed used for the DD4hepFatrasPGExample, all other material effects have been turned off.
For the Geant4 simulation the G4EmStandardPhysicsList.cpp had been used as a physics list, with all processes
turned off but the G4MuIonisation.
Parameters for both simulations:
particle type	   : mu-
particle mass	   : 105. MeV
momentum direction : (1.,0.,0.)
For different energies (as indicated in the file names) : 1 GeV, 10 GeV, 100 GeV

Using the output files of the two simulations and the script materialEffects.cpp files containing the delta X,Y,Z,E
distributions have been generated, using the following commands:

.L scripts/materialEffects.cpp
materialEffects("G4particleProperties_1GeV.root","G4particleProperties","g4MaterialEffects_1GeV.root",500,0.,1010.,0.,1.,0.,1.)
materialEffects("excells_charged.root","extrapolation_charged","actsMaterialEffects_500MeV.root",500,0.,1010.,0.,1.,0.,1.)

The geometry used was from the same input for both simulations (DD4hep) with a cylinder layer of 320mum of Silicon at 10.mm and the
deltas have been measured at 1 m.

The delta energy acts distributions had been slightly shifted to the geant4 distribution. Therefore scale factors of the most
probable value and the sigma of the distribution from fatras to Geant4 had been calculated.
This was done by plotting the dE distributions for different energies an comparing the offset of the sigma and the MOP
for geant4 and fatras. The obtained results can bee seen in scalorSigma.pdf and scalorMOP.pdf :

******************MOP**********************
Minimizer is Linear
Chi2                      =      2996.67
NDf                       =           11
p0                        =     0.745167   +/-   0.00476466

*****************Sigma***********************
Minimizer is Linear
Chi2                      =      4506.25
NDf                       =           11
p0                        =      0.68925   +/-   0.0058428

The plots have been generated using the following commands in ROOT:

std::vector<std::string> actsFiles = {"actsMaterialEffects_500MeV.root","actsMaterialEffects_1GeV.root","actsMaterialEffects_10GeV.root","actsMaterialEffects_20GeV.root","actsMaterialEffects_30GeV.root","actsMaterialEffects_40GeV.root","actsMaterialEffects_50GeV.root","actsMaterialEffects_60GeV.root","actsMaterialEffects_70GeV.root","actsMaterialEffects_80GeV.root","actsMaterialEffects_90GeV.root","actsMaterialEffects_100GeV.root"}
std::vector<std::string> g4Files = {"g4MaterialEffects_500MeV.root","g4MaterialEffects_1GeV.root","g4MaterialEffects_10GeV.root","g4MaterialEffects_20GeV.root","g4MaterialEffects_30GeV.root","g4MaterialEffects_40GeV.root","g4MaterialEffects_50GeV.root","g4MaterialEffects_60GeV.root","g4MaterialEffects_70GeV.root","g4MaterialEffects_80GeV.root","g4MaterialEffects_90GeV.root","g4MaterialEffects_100GeV.root"}
std::vector<float> momenta = {500,1000,10000, 20000,30000,40000,50000,60000,70000,80000,90000,100000}
.L scripts/compareEnergyLoss.cpp
compareEnergyLoss(g4Files,actsFiles,momenta,500,0.,0.5,1000,0.,0.1)

A final comparison between geant4 & acts (including the scalors) for different material thicknesses (100mum,320mum,1mm) can be seen in the plots EnergyLoss.pdf, EnergyLoss1.pdf, EnergyLoss_log.pdf, EnergyLoss_log1.pdf. This plot was produced using the root script compareMaterialEffects.cpp:

std::vector<std::string> g4 = {"g4MaterialEffects_10GeV_100mum.root","g4MaterialEffects_10GeV.root","g4MaterialEffects_10GeV_1mm.root"}
std::vector<std::string> acts = {"validation/eLoss/mu/actsMaterialEffects_wScalors_10GeV_100mum.root","validation/eLoss/mu/actsMaterialEffects_wScalors_10GeV.root","validation/eLoss/mu/actsMaterialEffects_wScalors_10GeV_1mm.root"}
std::vector<std::string> thicknesses = {"100 #mum","320 #mum", "1 mm"}
.L scripts/compareMaterialEffects.cpp
compareMaterialEffects(g4,acts,thicknesses,”Material Thickness","dE","EnergyLoss due to ionization")


If one wants to compare one Geant4 histogram to one ACTS histogram the script errorOf1DHistograms.cpp can be used:
.L scripts/errorOfHistograms.cpp
errorOf1DHistograms("g4MaterialEffects_10GeV.root","dE","Geant4","actsMaterialEffects_wScalors_10GeV.root","dE","ACTS")


 --------------------------------******************************-------------------------------------
Detector geometry xml file:
 --------------------------------******************************-------------------------------------
<?xml version="1.0" encoding="UTF-8"?>
<lccdd xmlns:compact="http://www.lcsim.org/schemas/compact/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xs:noNamespaceSchemaLocation="http://www.lcsim.org/schemas/compact/1.0/compact.xsd">
    
    <includes>
        <gdmlFile  ref="elements.xml"/>
        <gdmlFile  ref="materials.xml"/>
    </includes>
    
    <info name="FCCTracker"
        title="A test tracker in FCC dimensions"
        author="julia.hrdinka@cern.ch"
        url="none"
        status="development"
        version="$Id:??$">
        <comment>Dummy detector consisting of one cylindrical material layer and a measurement layer.</comment>
    </info>
    
    <define>
    <!--World-->
        <constant name="world_size" value="1010.*mm"/>
        <constant name="world_x" value="world_size"/>
        <constant name="world_y" value="world_size"/>
        <constant name="world_z" value="world_size"/>
        
        <constant name="MaterialTube_rmin" value="0.*mm"/>
        <constant name="MaterialTube_rmax" value="1000.*mm"/>
        <constant name="MaterialTube_length" value="10000.*mm"/>
        
        <constant name="Layer_rmin" value="10*mm"/>
        <constant name="Layer_rmax" value="10.32*mm"/>
        <constant name="Layer_length" value="10000.*mm"/>
        
        <constant name="OutsideTube_rmin" value="50.*mm"/>
        <constant name="OutsideTube_rmax" value="1000.*mm"/>
        <constant name="OutsideTube_length" value="10000.*mm"/>
        
        <constant name="Layer1_rmin" value="998.*mm"/>
        <constant name="Layer1_rmax" value="999.*mm"/>
        <constant name="Layer1_length" value="10000.*mm"/>   
    </define>
    
    <display>
        <vis name="violet" r="0.4" g="0." b="0.4" alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="red" r="0.6" g="0." b="0." alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="orange" r="0.65" g="0.35" b="0." alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="blue" r="0." g="0." b="0.5" alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="green" r="0." g="0.5" b="0." alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="BlueVisTrans" alpha="0.1" r="0.0" g="0.0" b="1.0" showDaughters="true" visible="false"/>
    </display>
    
    <detectors>
    <!--SimpleTube-->
        <detector id="0" name="MaterialTube" type="ACTS_Barrel">
            <dimensions rmin="MaterialTube_rmin" rmax="MaterialTube_rmax" dz="MaterialTube_length" z="0.*mm" vis="invisible" material="Vacuum"/>
            <layer id="0" inner_r="Layer_rmin" outer_r="Layer_rmax" z="Layer_length" material="Silicon" vis="orange"/>  
            <layer id="1" inner_r="Layer1_rmin" outer_r="Layer1_rmax" z="Layer1_length" material="Vacuum" vis="orange"/>
        </detector>
    </detectors>
</lccdd>

----------------------------------******************************-------------------------------------
materialEffects.cpp
----------------------------------******************************-------------------------------------
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TROOT.h"
#include "TTree.h"

/// This script prints histograms of the displacement, change in momentum and
/// energy loss.
/// It was created in order to validate Fatras simulation against Geant4.
/// It should can used with the Output of the RootExCellWriter and the
/// MaterialEffectsWriter.

void
materialEffects(std::string inFile,
                std::string treeName,
                std::string outFile,
                int         nBins,
                float       xMin,
                float       xMax,
                float       yMin,
                float       yMax,
                float       zMin,
                float       zMax)
{
  std::cout << "Opening file: " << inFile << std::endl;
  TFile inputFile(inFile.c_str());
  std::cout << "Reading tree: " << treeName << std::endl;
  TTree* tree = (TTree*)inputFile.Get(treeName.c_str());

  TTreeReader reader(treeName.c_str(), &inputFile);

  float dx;
  float dy;
  float dz;

  float dPx;
  float dPy;
  float dPz;

  tree->SetBranchAddress("dx", &dx);
  tree->SetBranchAddress("dy", &dy);
  tree->SetBranchAddress("dz", &dz);

  tree->SetBranchAddress("dPx", &dPx);
  tree->SetBranchAddress("dPy", &dPy);
  tree->SetBranchAddress("dPz", &dPz);

  std::cout << "Creating new output file: " << outFile
            << " and writing out histograms. " << std::endl;
  TFile outputFile(outFile.c_str(), "recreate");

  TH1F* dx_hist = new TH1F("dx_hist", "Displacement in x", nBins, xMin, xMax);
  TH1F* dy_hist = new TH1F("dy_hist", "Displacement in y", nBins, yMin, yMax);
  TH1F* dz_hist = new TH1F("dz_hist", "Displacement in z", nBins, zMin, zMax);

  TH1F* dE_hist = new TH1F("dE", "EnergyLoss", nBins, 0., 1.);

  Int_t entries = tree->GetEntries();

  for (int i = 0; i < entries; i++) {
    tree->GetEvent(i);
    dx_hist->Fill(dx);
    dy_hist->Fill(dy);
    dz_hist->Fill(dz);
    float dE = sqrt(dPx * dPx + dPy * dPy + dPz * dPz);
    dE_hist->Fill(dE);
  }

  dx_hist->GetXaxis()->SetTitle("dx");
  dx_hist->GetYaxis()->SetTitle("#particles");

  dy_hist->GetXaxis()->SetTitle("dy");
  dy_hist->GetYaxis()->SetTitle("#particles");

  dz_hist->GetXaxis()->SetTitle("dz");
  dz_hist->GetYaxis()->SetTitle("#particles");

  dE_hist->GetXaxis()->SetTitle("dE");
  dE_hist->GetYaxis()->SetTitle("#particles");

  dx_hist->Write();
  dy_hist->Write();
  dz_hist->Write();

  dE_hist->Write();

  delete dx_hist;
  delete dy_hist;
  delete dz_hist;

  delete dE_hist;

  inputFile.Close();
  outputFile.Close();
}

----------------------------------******************************-------------------------------------
compareEnergyLoss.cpp
----------------------------------******************************-------------------------------------
#include <iterator>
#include <tuple>
#include "TFile.h"
#include "TH1F.h"
#include "TROOT.h"
#include "TTree.h"

void
compareEnergyLoss(std::vector<std::string> dEHistFile_g4,
                  std::vector<std::string> dEHistFile_acts,
                  std::vector<float>       momenta,
                  size_t                   mp_nBins,
                  float                    mp_min,
                  float                    mp_max,
                  size_t                   s_nBins,
                  float                    s_min,
                  float                    s_max)
{
  // G4
  TH2F* mp_g4 = new TH2F("mp_g4",
                         "Most Probable Value of Energyloss",
                         1000,
                         100.,
                         100100.,
                         mp_nBins,
                         mp_min,
                         mp_max);
  TH2F* s_g4 = new TH2F("s_g4",
                        "Standard Deviation of Energyloss",
                        1000,
                        100.,
                        100100.,
                        s_nBins,
                        s_min,
                        s_max);

  TH2F* mp_acts = new TH2F("mp_acts",
                           "Most Probable Value of Energyloss",
                           1000,
                           100.,
                           100100.,
                           mp_nBins,
                           mp_min,
                           mp_max);
  TH2F* s_acts = new TH2F("s_acts",
                          "Standard Deviation of Energyloss",
                          1000,
                          100.,
                          100100.,
                          s_nBins,
                          s_min,
                          s_max);

  TH2F* scalor = new TH2F("scalor",
                          "Scalor from Fatras to Geant4 for #sigma(dE)",
                          1000,
                          100.,
                          100100.,
                          s_nBins,
                          0.,
                          1.);

  TH2F* scalorMOP = new TH2F("scalor",
                             "Scalor from Fatras to Geant4 for MOP(dE)",
                             1000,
                             100.,
                             100100.,
                             s_nBins,
                             0.,
                             1.);

  if (dEHistFile_g4.size() != dEHistFile_acts.size()
      || dEHistFile_g4.size() != momenta.size()
      || dEHistFile_acts.size() != momenta.size()) {
    std::cout << "ERROR, Vectors need to have same size!" << std::endl;
    return;
  }

  for (size_t it = 0; it < momenta.size(); it++) {
    std::cout << "Opening file: " << dEHistFile_g4.at(it).c_str() << std::endl;
    TFile inputFile_g4(dEHistFile_g4.at(it).c_str());
    TH1F* input_g4 = (TH1F*)inputFile_g4.Get("dE");

    std::cout << "Opening file: " << dEHistFile_acts.at(it).c_str()
              << std::endl;
    TFile inputFile_acts(dEHistFile_acts.at(it).c_str());
    TH1F* input_acts = (TH1F*)inputFile_acts.Get("dE");

    input_g4->Fit("landau");
    input_acts->Fit("landau");

    float mostPropG4   = input_g4->GetFunction("landau")->GetParameter(1);
    float mostPropActs = input_acts->GetFunction("landau")->GetParameter(1);

    float sigmaG4   = input_g4->GetFunction("landau")->GetParameter(2);
    float sigmaActs = input_acts->GetFunction("landau")->GetParameter(2);
    float momentum  = momenta.at(it);

    float scalFactor = sigmaG4 / sigmaActs;
    float scalMean   = mostPropG4 / mostPropActs;

    std::cout << "----Geant4----" << std::endl;
    std::cout << "mp: " << mostPropG4 << ", sigma: " << sigmaG4 << std::endl;
    std::cout << "----Acts----" << std::endl;
    std::cout << "Scalor: " << scalFactor << std::endl;
    std::cout << "momentum: " << momentum << std::endl;

    mp_g4->Fill(momentum, mostPropG4);
    s_g4->Fill(momentum, sigmaG4);

    mp_acts->Fill(momentum, mostPropActs);
    s_acts->Fill(momentum, sigmaActs);

    scalor->Fill(momentum, scalFactor);
    scalorMOP->Fill(momentum, scalMean);

    inputFile_g4.Close();
    inputFile_acts.Close();
  }

  //  TFile outputFile(outFile.c_str(), "recreate")

  // make a common canvas
  TCanvas* c1 = new TCanvas();
  gStyle->SetOptStat(0);
  c1->Divide(1, 2);
  c1->cd(1);

  mp_g4->SetMarkerColor(1);
  mp_g4->SetLineColor(1);
  mp_g4->SetMarkerStyle(34);
  mp_g4->GetXaxis()->SetTitle("momentum [MeV]");
  mp_g4->GetYaxis()->SetTitle("MOP of dE");
  mp_g4->Draw("");

  mp_acts->SetMarkerColor(2);
  mp_acts->SetLineColor(2);
  mp_acts->SetMarkerStyle(34);
  mp_acts->GetXaxis()->SetTitle("momentum [MeV]");
  mp_acts->GetYaxis()->SetTitle("MOP of dE");
  mp_acts->Draw("same");

  TLegend* leg = new TLegend(0.72, 0.696, 0.99, 0.936);
  leg->AddEntry(mp_g4, "g4");
  leg->AddEntry(mp_acts, "acts");
  leg->Draw();
  mp_g4->SetDirectory(0);
  mp_acts->SetDirectory(0);

  // second pad
  c1->cd(2);
  scalorMOP->Fit("pol0");
  // scalor->Divide(s_g4);
  scalorMOP->SetMarkerColor(4);
  scalorMOP->SetMarkerStyle(34);
  scalorMOP->GetXaxis()->SetTitle("momentum [MeV]");
  scalorMOP->GetYaxis()->SetTitle("scalor");
  scalorMOP->Draw("same");
  scalorMOP->SetDirectory(0);

  TCanvas* c2 = new TCanvas();
  c2->Divide(1, 2);
  c2->cd(1);
  s_g4->SetMarkerColor(1);
  s_g4->SetLineColor(1);
  s_g4->SetMarkerStyle(34);
  s_g4->GetXaxis()->SetTitle("momentum [MeV]");
  s_g4->GetYaxis()->SetTitle("#sigma(dE)");
  s_g4->Draw("");
  s_acts->SetMarkerColor(2);
  s_acts->SetLineColor(2);
  s_acts->SetMarkerStyle(34);
  s_acts->GetXaxis()->SetTitle("momentum [MeV]");
  s_acts->GetYaxis()->SetTitle("#sigma(dE)");
  s_acts->Draw("same");
  TLegend* leg2 = new TLegend(0.72, 0.696, 0.99, 0.936);
  leg2->AddEntry(s_g4, "g4");
  leg2->AddEntry(s_acts, "acts");
  leg2->Draw();

  c2->cd(2);

  scalor->Fit("pol0");
  // scalor->Divide(s_g4);
  scalor->SetMarkerColor(4);
  scalor->SetMarkerStyle(34);
  scalor->GetXaxis()->SetTitle("momentum [MeV]");
  scalor->GetYaxis()->SetTitle("scalor");
  scalor->Draw("same");
  scalor->SetDirectory(0);
  s_g4->SetDirectory(0);
  s_acts->SetDirectory(0);
}

----------------------------------******************************-------------------------------------
----------------------------------******************************-------------------------------------
