26/06/17

Validation of multiple scattering of Fatras simulation compared to Geant4 simulation.
The Fatras plots (ExtrapolationChargedTest*.root) are the output of the DD4hepFatrasPGExample.cpp
and the Geant4 files (G4ParticleProperties*.root) are the output of the FatrasValidation.cpp example.
In order to only test multiple scattering only the MultipleScatteringSamplerHighland from the acts-fatras 
library has beed used for the DD4hepFatrasPGExample, all other material effects have been turned off.
For the Geant4 simulation the G4EmStandardPhysicsList.cpp had been used as a physics list, with all processes
turned off but the G4MuMultipleScattering.
Parameters for both simulations:
particle type      : mu-
particle mass      : 105. MeV
momentum direction : (1.,0.,0.)
For different energies (as indicated in the file names) : 1 GeV, 10 GeV, 100 GeV

Using the output files of the two simulations and the script materialEffects.cpp files containing the delta X,Y,Z,E 
distributions have been generated, using the following commands:

.L scripts/materialEffects.cpp
materialEffects("G4particleProperties_1GeV.root","G4particleProperties","g4MaterialEffects_1GeV.root",500,0.,1010.,0.,1.,0.,1.)
materialEffects("ExtrapolationChargedTest_1*GeV.root","ExtrapolationChargedTest","actsMaterialEffects_1*GeV.root",500,0.,1010.,0.,1.,0.,1.)

A final comparison between geant4 & acts for different transverse momenta (1 GeV, 10 GeV, 100 GeV) can be seen in the plots MSC.pdf, MSC1.pdf, MSC_log.pdf, MSC_log1.pdf. This plot was produced using the root script compareMaterialEffects.cpp:

std::vector<std::string> g4 = {"actsMaterialEffects_100GeV.root","actsMaterialEffects_10GeV.root","actsMaterialEffects_1GeV.root"}
std::vector<std::string> acts = {"actsMaterialEffects_100GeV.root","actsMaterialEffects_10GeV.root","actsMaterialEffects_1GeV.root"}
std::vector<std::string> momenta = {"100 GeV","10 GeV", "1 GeV"}
.L scripts/compareMaterialEffects.cpp
compareMaterialEffects(g4,acts,momenta,"pT","dz_hist","Displacement in z due to multiple scattering")


The geometry used was from the same input for both simulations (DD4hep) with a cylinder layer of 320mum of Silicon at 10.mm and the 
deltas have been measured at 1 m.
 --------------------------------******************************-------------------------------------
Detector Geometry xml file:
 --------------------------------******************************-------------------------------------
<?xml version="1.0" encoding="UTF-8"?>
<lccdd xmlns:compact="http://www.lcsim.org/schemas/compact/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xs:noNamespaceSchemaLocation="http://www.lcsim.org/schemas/compact/1.0/compact.xsd">
    
    <includes>
        <gdmlFile  ref="elements.xml"/>
        <gdmlFile  ref="materials.xml"/>
    </includes>
    
    <info name="FCCTracker"
        title="A test tracker in FCC dimensions"
        author="julia.hrdinka@cern.ch"
        url="none"
        status="development"
        version="$Id:??$">
        <comment>First demonstration tracker in FCC dimensions, containing simple silicon modules, without any substructure or support material. The example contains three hierarchies, a pixel tracker, a short strip and a long strip tracker.</comment>
    </info>
    
    <define>
    <!--World-->
        <constant name="world_size" value="1010.*mm"/>
        <constant name="world_x" value="world_size"/>
        <constant name="world_y" value="world_size"/>
        <constant name="world_z" value="world_size"/>
        
        <constant name="MaterialTube_rmin" value="0.*mm"/>
        <constant name="MaterialTube_rmax" value="1000.*mm"/>
        <constant name="MaterialTube_length" value="10000.*mm"/>
        
        <constant name="Layer_rmin" value="10.*mm"/>
        <constant name="Layer_rmax" value="10.32*mm"/>
        <constant name="Layer_length" value="10000.*mm"/>
        
        <constant name="OutsideTube_rmin" value="50.*mm"/>
        <constant name="OutsideTube_rmax" value="1000.*mm"/>
        <constant name="OutsideTube_length" value="10000.*mm"/>
        
        <constant name="Layer1_rmin" value="999.*mm"/>
        <constant name="Layer1_rmax" value="1001.*mm"/>
        <constant name="Layer1_length" value="10000.*mm"/>   
    </define>
    
    <display>
        <vis name="violet" r="0.4" g="0." b="0.4" alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="red" r="0.6" g="0." b="0." alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="orange" r="0.65" g="0.35" b="0." alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="blue" r="0." g="0." b="0.5" alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="green" r="0." g="0.5" b="0." alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="BlueVisTrans" alpha="0.1" r="0.0" g="0.0" b="1.0" showDaughters="true" visible="false"/>
    </display>
    
    <detectors>
    <!--SimpleTube-->
        <detector id="0" name="MaterialTube" type="ACTS_Barrel">
            <dimensions rmin="MaterialTube_rmin" rmax="MaterialTube_rmax" dz="MaterialTube_length" z="0.*mm" vis="invisible" material="Vacuum"/>
            <layer id="0" inner_r="Layer_rmin" outer_r="Layer_rmax" z="Layer_length" material="Silicon" vis="orange"/>  
        </detector>
    </detectors>
</lccdd>
 --------------------------------******************************-------------------------------------
materialEffects.cpp:
 --------------------------------******************************-------------------------------------
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TROOT.h"
#include "TTree.h"

void
materialEffects(std::string inFile,
                std::string treeName,
                std::string outFile,
                int         nBins,
                float       xMin,
                float       xMax,
                float       yMin,
                float       yMax,
                float       zMin,
                float       zMax)
{
  std::cout << "Opening file: " << inFile << std::endl;
  TFile inputFile(inFile.c_str());
  std::cout << "Reading tree: " << treeName << std::endl;
  TTree* tree = (TTree*)inputFile.Get(treeName.c_str());

  TTreeReader reader(treeName.c_str(), &inputFile);

  float dx;
  float dy;
  float dz;

  float dPx;
  float dPy;
  float dPz;

  tree->SetBranchAddress("dx", &dx);
  tree->SetBranchAddress("dy", &dy);
  tree->SetBranchAddress("dz", &dz);

  tree->SetBranchAddress("dPx", &dPx);
  tree->SetBranchAddress("dPy", &dPy);
  tree->SetBranchAddress("dPz", &dPz);

  std::cout << "Creating new output file: " << outFile
            << " and writing out histograms. " << std::endl;
  TFile outputFile(outFile.c_str(), "recreate");

  TH1F* dx_hist = new TH1F("dx_hist", "Displacement in x", nBins, xMin, xMax);
  TH1F* dy_hist = new TH1F("dy_hist", "Displacement in y", nBins, yMin, yMax);
  TH1F* dz_hist = new TH1F("dz_hist", "Displacement in z", nBins, zMin, zMax);

  TH1F* dE_hist = new TH1F("dE", "EnergyLoss", nBins, 0., 1.);

  Int_t entries = tree->GetEntries();

  for (int i = 0; i < entries; i++) {
    tree->GetEvent(i);
    dx_hist->Fill(dx);
    dy_hist->Fill(dy);
    dz_hist->Fill(dz);
    float dE = sqrt(dPx * dPx + dPy * dPy + dPz * dPz);
    //  std::cout << "dE: " << dE << std::endl;
    dE_hist->Fill(dE);
  }

  dx_hist->GetXaxis()->SetTitle("dx");
  dx_hist->GetYaxis()->SetTitle("#particles");

  dy_hist->GetXaxis()->SetTitle("dy");
  dy_hist->GetYaxis()->SetTitle("#particles");

  dz_hist->GetXaxis()->SetTitle("dz");
  dz_hist->GetYaxis()->SetTitle("#particles");

  dE_hist->GetXaxis()->SetTitle("dE");
  dE_hist->GetYaxis()->SetTitle("#particles");

  dx_hist->Write();
  dy_hist->Write();
  dz_hist->Write();

  dE_hist->Write();

  delete dx_hist;
  delete dy_hist;
  delete dz_hist;

  delete dE_hist;

  inputFile.Close();
  outputFile.Close();
}
 --------------------------------******************************-------------------------------------
19/09/2017

After major updates in the acts-framework, the same validation can now be run with the following command:
./build/bin/ACTFWFatrasGunDD4hep --dd4hep-input=file:Detectors/DD4hepDetector/compact/DummyDetector.xml --pg-phirange={0.,0.} --pg-etarange={0.,0.} --pg-ptrange={10000.,10000.} --events=100 --pg-nparticles=100

and afterwards generating the histograms in root by:

root [0] .L scripts/materialEffects.cpp
root [1] materialEffects("excells_charged.root","extrapolation_charged","actsMaterialEffects.root",500,0.,1010.,0.,1.,0.,1.)

It gave the same results as before.

The detector used was:
<?xml version="1.0" encoding="UTF-8"?>
<lccdd xmlns:compact="http://www.lcsim.org/schemas/compact/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xs:noNamespaceSchemaLocation="http://www.lcsim.org/schemas/compact/1.0/compact.xsd">
    
    <includes>
        <gdmlFile  ref="elements.xml"/>
        <gdmlFile  ref="materials.xml"/>
    </includes>
    
    <info name="FCCTracker"
        title="A test tracker in FCC dimensions"
        author="julia.hrdinka@cern.ch"
        url="none"
        status="development"
        version="$Id:??$">
        <comment>First demonstration tracker in FCC dimensions, containing simple silicon modules, without any substructure or support material. The example contains three hierarchies, a pixel tracker, a short strip and a long strip tracker.</comment>
    </info>
    
    <define>
    <!--World-->
        <constant name="world_size" value="1010.*mm"/>
        <constant name="world_x" value="world_size"/>
        <constant name="world_y" value="world_size"/>
        <constant name="world_z" value="world_size"/>
        
        <constant name="MaterialTube_rmin" value="0.*mm"/>
        <constant name="MaterialTube_rmax" value="1000.*mm"/>
        <constant name="MaterialTube_length" value="10000.*mm"/>
        
        <constant name="Layer_rmin" value="10*mm"/>
        <constant name="Layer_rmax" value="10.32*mm"/>
        <constant name="Layer_length" value="10000.*mm"/>
        
        <constant name="OutsideTube_rmin" value="50.*mm"/>
        <constant name="OutsideTube_rmax" value="1000.*mm"/>
        <constant name="OutsideTube_length" value="10000.*mm"/>
        
        <constant name="Layer1_rmin" value="999.*mm"/>
        <constant name="Layer1_rmax" value="1001.*mm"/>
        <constant name="Layer1_length" value="10000.*mm"/>   
    </define>
    
    <display>
        <vis name="violet" r="0.4" g="0." b="0.4" alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="red" r="0.6" g="0." b="0." alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="orange" r="0.65" g="0.35" b="0." alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="blue" r="0." g="0." b="0.5" alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="green" r="0." g="0.5" b="0." alpha="1" showDaugthers="true" visible="true" drawingStyle="solid"/>
        <vis name="BlueVisTrans" alpha="0.1" r="0.0" g="0.0" b="1.0" showDaughters="true" visible="false"/>
    </display>
    
    <detectors>
    <!--SimpleTube-->
        <detector id="0" name="MaterialTube" type="ACTS_Barrel">
            <dimensions rmin="MaterialTube_rmin" rmax="MaterialTube_rmax" dz="MaterialTube_length" z="0.*mm" vis="invisible" material="Vacuum"/>
            <layer id="0" inner_r="Layer_rmin" outer_r="Layer_rmax" z="Layer_length" material="Silicon" vis="orange"/>  
            <layer id="1" inner_r="Layer1_rmin" outer_r="Layer1_rmax" z="Layer1_length" material="Vacuum" vis="orange"/>
        </detector>
    </detectors>
</lccdd>

