# A Common Tracking Software (ACTS) Project - Data Repository {#mainpage}
This repository contains data needed to run examples in Acts.

1. [MagneticField](#bfield)
2. [Fatras Validation] (#fatars)

# <a name="bfield">MagneticField</a>
This directory contains magnetic field maps of ATLAS and FCChh in txt/csv 
and root format.

# <a name="fatras">Fatras Validation</a>
This directory contains the validation of the Fatras material effects multiple scattering (msc), energy loss (eLoss) and combined (eLoss_msc) for muons against Geant4.
